#!/bin/bash
# 
# Try to detect which is the right Wikimedia bastion to use
# and update local ssh config if it is not matching.
#

# adjust this if your SSH config is in a non-standard place
SSH_CONFIG="${HOME}/.ssh/config"

# find out which data center is local for the user
function get_local_dc()
{
    # get the IP of wikipedia.org to use GeoDNS based on location
    WIKIPEDIA_IP=$(host -t A wikipedia.org | cut -d " " -f 4)

    # resolve the IP and parse to get data center name
    DATA_CENTER=$(host $WIKIPEDIA_IP | cut -d. -f 7)

    echo $DATA_CENTER
}


# find the bastions currently listed on Wikitech
function get_bastions()
{
    SOURCE_OF_TRUTH="https://wikitech.wikimedia.org/w/api.php?action=parse&page=Bastion&format=json"
    CURL="/usr/bin/curl -s"

    BASTIONS=$(${CURL} ${SOURCE_OF_TRUTH} | grep -Eo 'bast[1-9][0-9][0-9][0-9]' | sort | uniq | xargs)
    echo $BASTIONS
}


# match a data center to the current bastion for it
function match_bastion () {
    DATA_CENTER=$1
    case $DATA_CENTER in
        'eqiad')
            prefix="1"
            ;;
        'codfw')
            prefix="2"
            ;;
        'esams')
            prefix="3"
            ;;
        'ulsfo')
            prefix="4"
            ;;
        'eqsin')
            prefix="5"
            ;;
    esac

    BASTION=$(echo $BASTIONS | grep -o "bast${prefix}[0-9][0-9][0-9]")

    echo $BASTION
}

# which bastion is currently configured locally
function get_configured_bastion() {
        
    grep ProxyCommand ${SSH_CONFIG} | grep -o "bast[1-9][0-9][0-9][0-9]"

}

# attempt to fix ssh config
# replace bastion names but only on lines with ProxyCommand
function fix_config() {
        echo "Ok. Replacing ${IS_BASTION} with ${SHOULD_BASTION} in ${SSH_CONFIG}."
        sed -i -e "/ProxyCommand/ s/${IS_BASTION}/${SHOULD_BASTION}/g" $SSH_CONFIG
}

LOCAL_DC=$(get_local_dc)
echo -e "Your local data center is: ${LOCAL_DC}\n"

BASTIONS=$(get_bastions)
echo -e "Bastions found on Wikitech: ${BASTIONS}\n"

SHOULD_BASTION=$(match_bastion $LOCAL_DC)
echo -e "You should use: ${SHOULD_BASTION}.wikimedia.org\n"

IS_BASTION=$(get_configured_bastion)
echo -e "Your configured bastion is: ${IS_BASTION}.wikimedia.org\n"

if [ "$IS_BASTION" != "$SHOULD_BASTION" ]; then
    echo "The configured bastion does not match the recommended bastion."
    read -p "Should i try to fix? (y/n)" -n 1 -r
    echo    
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        fix_config
        IS_BASTION=$(get_configured_bastion)
        echo -e "Your configured bastion is now: ${IS_BASTION}.wikimedia.org\n"
    fi
fi

